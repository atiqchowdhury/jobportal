The job matching canndidate system has been built using C# for the backend and React for the frontend.

PREREQUISITES:
•	Visual Studio 2019 or later 
•	NET Framework 4.7.2
•	Node.js

QUICK START: 
•	Verify prerequisites above
•	Clone the project from GitLab
•	Open a command prompt
•	If npm is not installed, type "npm install".  Otherwise type "npm run build"
•	To run, browse to the selected folder in the command prompt and type 'npm run start"

HOW IT WORKS:
When the page loads initially, all the jobs are displayed. Each job has a set of skill tags.  Each one of these skill tags are clickable.  When a tags is clicked from the frontend, the jobs collection result set is filtered based on the selected skill tag.  The clicked skill tag are copied to the search box.  There is a placeholder to indicate how many matching candidates are found against each job based on the skill tag selected.  Please note that this is only a placeholder and the functionality to calculate the matching candidates needs to be completed.

TODO:
Due to time constraints, all the functionality is not completed.  THe business logic to find no of matching candidates based on skill tags is done. But it needs to be passed on from the controlled to the frontend through a webservice call.

HOW ARE CANDIDATES MATCHED TO A JOB:
Search params are passed to a Controller called "SearchCandidatesController".  THe controlled loads both the candidates and jobs JSON file and deserealises the data.  Dictionary is used to store the jobid and number of candidates as key value pair.  The Dictionary only stores the total count of matching candidates against each job.  

Once the search param of skilltags are passed to the controller, following steps are taken
1) find the job that has any of the skill tag 
2) If jobs are found that matches the search criteria, then find candidates that has any of the skill tag.
3) Increment total no of matching candidates
4) Store the JobId and Total no of matching candidates in Dictionary.
5) Reset Total no of matching candidates= 0 and move to the second Job ID and so forth.
